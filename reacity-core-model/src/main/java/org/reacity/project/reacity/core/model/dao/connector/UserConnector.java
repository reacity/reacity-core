/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.reacity.project.reacity.core.model.dao.connector;

import org.reacity.project.reacity.core.model.dao.User;
import org.reacity.project.reacity.core.model.dao.ZoneInformation;
import org.reacity.project.reacity.core.model.dao.exception.CommonReacityModelException;

/**
 *
 * @author alexandre
 */
public interface UserConnector {

    User createUser(String mail, String password, String name) throws CommonReacityModelException;

    User getUser(String mail) throws CommonReacityModelException;

    void defineUserRole(User user, ZoneInformation zoneInformation) throws CommonReacityModelException;
}
