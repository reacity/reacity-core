/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.reacity.project.reacity.core.model.dao.connector;

import org.reacity.project.reacity.core.model.dao.ZoneInformation;
import org.reacity.project.reacity.core.model.dao.exception.CommonReacityModelException;

/**
 *
 * @author alexandre
 */
public interface ZoneInformationConnector {

    ZoneInformation createZoneInformation(float latitude, float longitude, int radius, String information) throws CommonReacityModelException;
    
    ZoneInformation updateZoneInformation(ZoneInformation zoneInformation) throws CommonReacityModelException;
    
    void deleteZoneInformation(ZoneInformation zoneInformation) throws CommonReacityModelException;
    
    ZoneInformation getZoneInformation(float latitude, float longitude) throws CommonReacityModelException;

}
