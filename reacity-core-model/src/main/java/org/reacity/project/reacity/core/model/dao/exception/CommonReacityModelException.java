/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.reacity.project.reacity.core.model.dao.exception;

/**
 *
 * @author alexandre
 */
public class CommonReacityModelException extends Exception{

    public CommonReacityModelException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
