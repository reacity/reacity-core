/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.reacity.project.reacity.core.android;

import org.reacity.project.reacity.core.exception.CommonReacityCoreException;

/**
 *
 * @author alexandre
 */
public interface InformationRequest {
    
    String getLocalInformation(float latitude, float longitude) throws CommonReacityCoreException;
    
}
