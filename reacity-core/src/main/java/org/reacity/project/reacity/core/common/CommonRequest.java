/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.reacity.project.reacity.core.common;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 * @author alexandre
 */
@Path("/")
public class CommonRequest {

    @GET
    @Path("/isAlive")
    public String isAlive() {
        return "Server is alive";
    }
}
