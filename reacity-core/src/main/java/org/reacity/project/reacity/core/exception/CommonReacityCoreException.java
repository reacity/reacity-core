/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.reacity.project.reacity.core.exception;

/**
 *
 * @author alexandre
 */
public class CommonReacityCoreException extends Exception {

    public CommonReacityCoreException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
