/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.reacity.project.reacity.core.front;

import org.reacity.project.reacity.core.exception.CommonReacityCoreException;
import org.reacity.project.reacity.core.model.dao.User;

/**
 *
 * @author alexandre
 */
public interface UserRequest {
    
    void signup(String mail, String name, String password) throws CommonReacityCoreException;
    
    User login(String mail, String password) throws CommonReacityCoreException;
}
