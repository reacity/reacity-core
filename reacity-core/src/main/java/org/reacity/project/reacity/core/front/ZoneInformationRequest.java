/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.reacity.project.reacity.core.front;

import org.reacity.project.reacity.core.exception.CommonReacityCoreException;
import org.reacity.project.reacity.core.model.dao.ZoneInformation;

/**
 *
 * @author alexandre
 */
public interface ZoneInformationRequest {

    ZoneInformation addInformation(ZoneInformation zoneInformation) throws CommonReacityCoreException;

    ZoneInformation updateInformation(ZoneInformation zoneInformation) throws CommonReacityCoreException;
    
    ZoneInformation deleteInformation(ZoneInformation zoneInformation) throws CommonReacityCoreException;
}
